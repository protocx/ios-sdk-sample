//
//  ViewController.swift
//  ProtoChatSample
//
//  Created by Le Dinh Tuan on 22/09/2021.
//

import UIKit
import ProtoChat

class ViewController: UIViewController {
    
    @objc func buttonAction(sender: UIButton!) {
        
//        let storyboard = UIStoryboard(
//            name: "Main", bundle: Bundle(for: Proto.self)
//        )
//        if let chatListVC = storyboard.instantiateInitialViewController() as UIViewController? {
//            self.present(chatListVC, animated: true, completion: nil)
//        }
        if let vc = Proto.getInstance().getMainViewController() {
            self.present(vc, animated: true, completion: nil)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let button = UIButton()
        view.addSubview(button)
        button.setTitleColor(UIColor.yellow, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Start", for: .normal)
        button.backgroundColor = UIColor.gray
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
       
        button.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        button.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        button.widthAnchor.constraint(equalTo: view.widthAnchor, constant: 100).isActive = true
        button.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
    }

}

