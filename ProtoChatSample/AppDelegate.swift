//
//  AppDelegate.swift
//  ProtoChatSample
//
//  Created by Le Dinh Tuan on 22/09/2021.
//

import UIKit
import ProtoChat
import FirebaseMessaging

@main
class AppDelegate: UIResponder, MessagingDelegate, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    var TAG = "ProtoChatSample:AppDelegate - "
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        Proto.getInstance().messaging(messaging, didReceiveRegistrationToken: fcmToken)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {

        Proto.getInstance().application(didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
//        print(TAG + "didReceiveRemoteMsg: " )
        if let message = userInfo[AnyHashable("json_result")] as? String {
            print(TAG + message)
        }
        Proto.getInstance().application(didReceiveRemoteNotification: userInfo)
    }
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // init Proto by setting gateway/channel id
        Proto.initialize(application, gatewayId: "gw_YAtSB7l1lHxUfdEG2")
        
        print(TAG + "fcm cache: " + Proto.getInstance().getFirebaseToken())
        
//        if #available(iOS 10.0, *) {
//          // For iOS 10 display notification (sent via APNS)
//          UNUserNotificationCenter.current().delegate = self
//
//          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//          UNUserNotificationCenter.current().requestAuthorization(
//            options: authOptions,
//            completionHandler: { _, _ in }
//          )
//        } else {
//          let settings: UIUserNotificationSettings =
//            UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//          application.registerUserNotificationSettings(settings)
//        }
//
//        application.registerForRemoteNotifications()

        // Uncomment section below to custom style
//        ProtoStyle.userAvatarBG = "#666666"
//        ProtoStyle.chatPrimary = "#000000"
//        ProtoStyle.chatPrimaryFocus = "#000000"
//        ProtoStyle.chatPrimaryPressed = "#000000"
//        ProtoStyle.chatPrimaryDisabled = "#66000000"
//        ProtoStyle.chatBackground = "#FFFFFF"
//        ProtoStyle.chatBackgroundInput = "#FFFFFF"
//        ProtoStyle.chatSecondary = "#FFFFFF"
//        ProtoStyle.chatSecondaryFocus = "#F1F1F1"
//        ProtoStyle.chatSecondaryPressed = "#E9E9E9"
//        ProtoStyle.chatSecondaryDisabled = "#66FFFFFF"
//        ProtoStyle.botBGBubbleStroke = "#f5f5f8"
//        ProtoStyle.botBGBubble = "#f5f5f8"
//        ProtoStyle.botText = "#4a4a4a"
//        ProtoStyle.userBGBubbleStroke = "#000000"
//        ProtoStyle.userBGBubble = "#000000"
//        ProtoStyle.userText = "#ffffff"
//        ProtoStyle.checkbox = "#161616"
//        ProtoStyle.chatboxHintText = "#d4d4d4"
//        ProtoStyle.chatboxDivider = "#ececec"
//        ProtoStyle.dateTimeText = "#d4d4d4"
//        ProtoStyle.sendButton = "#d4d4d4"
//        ProtoStyle.statusBarColor = "#00FFFFFF"
//        ProtoStyle.cardSelectedBg = "#000000"
//        ProtoStyle.newChatButton = "#f9c927"
//        ProtoStyle.botNameColor = "#9c9c9c"
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
}

